import { useRouter } from 'next/router'

import Head from 'next/head'

export default function AboutWho({ whoseAbout }) {
  const router = useRouter()
  const { who } = router.query
  
  return (
    <>
    <Head>
      {/** SEO stuff */}
      <title>{who}</title>
    </Head>
      <h1>About {who}</h1>
      <p>this is about {whoseAbout.id} : {whoseAbout.description}</p>
    </>
  )
}

export async function getStaticProps({ params }) {
  const req = await fetch(`http://localhost:3000/${params.who}.json`)
  const data = await req.json()

  return {
    props: { whoseAbout: data }
  }
}

export async function getStaticPaths() {
  const req = await fetch('http://localhost:3000/about.json')
  const data = await req.json()

  const paths = data.map(whose => {
    return { params: { who: whose }}
  })

  return {
    paths,
    fallback: false
  }
}

export async function getServerSide() {
  return
}