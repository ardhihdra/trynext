
export default function About({ paths }) {
  return (
    <>
      <div>Who do you want to see?</div>
      {
        paths.map(pt => {
          const path = `/about/${pt}`
          return (
            <>
              <a href={path}>{pt}</a><br/>
            </>
          )
        })
      }
    </>
  )
}

export async function getStaticProps({}) {
  const req = await fetch('http://localhost:3000/about.json')
  const data = await req.json()

  return {
    props: { paths: data }
  }
}